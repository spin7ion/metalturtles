package org.spin7ion.MetalTutles.controlElements;

import org.spin7ion.MetalTurtles.graphics.BitmapObject;

import android.graphics.Bitmap;
import android.util.DisplayMetrics;

public class ControlElement extends BitmapObject {
	private ControlElementOnClickListener myOnclickListener;
	private DisplayMetrics mDisplayMetrics;
	public ControlElement(Bitmap graphics, float x, float y, DisplayMetrics dm) {
		super(graphics, x, y);
		this.setAbsolute();
		mDisplayMetrics=dm;
	}
	
	public boolean checkClick(int x,int y){
		if(isPointCollide(x,y,mDisplayMetrics)){
			myOnclickListener.onControlElementClick(new ControlElementEvent(this,x,y));
			return true;
		}
		return false;
	}
	
	public void setControlElementOnClickListener(ControlElementOnClickListener listener){
		myOnclickListener=listener;
	}
	public ControlElementOnClickListener getControlElementOnClickListener(){
		return myOnclickListener;
	}

}
