package org.spin7ion.MetalTutles.controlElements;

import org.spin7ion.MetalTurtles.graphics.Camera;
import org.spin7ion.MetalTurtles.graphics.BitmapObject;
import org.spin7ion.Utils.GeometryUtils;
import org.spin7ion.Utils.MathVector;
import org.spin7ion.MetalTurtles.R;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

public class TankMoveElement extends BitmapObject {

	private BitmapObject gun;
	
	private BitmapObject orderGun;//order to rotate gun
	//private BitmapObject orderFire;//order to fire
	
	private float angle=90.0f;
	
	private float bodyAngle=0.f;
	
	private boolean isBodyTurnMade=false;
	private boolean isGunTurnMade=false;
	private boolean isMoveMade=false;
	
	
	public float getGunAngle() {
		return angle;
	}
	public float getBodyAngle() {
		return bodyAngle;
	}
	private float maxDistance;
	
	private static float radius=56.0f;
	
	public TankMoveElement(Resources res,float maxDistance) {
		super(BitmapFactory.decodeResource(res,
				R.drawable.tankorder), 0, 0,28,28);
		gun=new BitmapObject(BitmapFactory.decodeResource(res,
				R.drawable.tankordergun), 0, 0,7.5f,24.5f);
		
		orderGun=new BitmapObject(BitmapFactory.decodeResource(res,
				R.drawable.tankordergunorder), 0, 0,14.5f,18.5f);
		this.maxDistance=maxDistance;
		moveGun();		
		moveOrderGun(getX(),getY()-10); 
	}
	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		super.draw(myCanvas, myCamera, metrics);

		gun.draw(myCanvas, myCamera, metrics);
		
		orderGun.draw(myCanvas, myCamera, metrics);
		
	}
	public void move(float x, float y,float tankX,float tankY) {
		bodyAngle=GeometryUtils.getAngleDeg(x, y, tankX, tankY);
		if(Math.hypot(x-tankX, y-tankY)>maxDistance){
			x=tankX+(float) Math.cos(GeometryUtils.degToRad(bodyAngle)-Math.PI/2)*maxDistance;
			y=tankY+(float) Math.sin(GeometryUtils.degToRad(bodyAngle)-Math.PI/2)*maxDistance;
		}
		super.setAngle(bodyAngle);
		super.move(x, y);		
		moveGun();		
		moveOrderGun();
	}
	public void updateBodyAngle(float tankX,float tankY){
		if(Math.abs(x-tankX)>10 && Math.abs(y-tankY)>10){
		bodyAngle=GeometryUtils.getAngleDeg(x, y, tankX, tankY);
		super.setAngle(bodyAngle);
		}
	}
	public void finishTurn(){
		isBodyTurnMade=true;
		isGunTurnMade=true;
		isMoveMade=true;
	}
	public void startTurn(){
		isBodyTurnMade=false;
		isGunTurnMade=false;
		isMoveMade=false;
	}
	public boolean isTurnMade(){
		return isBodyTurnMade && isGunTurnMade && isMoveMade;
	}
	public boolean isBodyTurnMade(){
		return isBodyTurnMade;
	}
	public void setBodyTurnMade(boolean a){
		isBodyTurnMade=a;
	}
	public boolean isGunTurnMade(){
		return isGunTurnMade;
	}
	public void setGunTurnMade(boolean a){
		isGunTurnMade=a;
	}
	public boolean isMoveMade(){
		return isMoveMade;
	}
	public void setMoveMade(boolean a){
		isMoveMade=a;
	}
	
	private void rotateOrderGun(float angle){		
		orderGun.setAngle(angle);
		gun.setAngle(angle);
	}
	private void moveGun() {		
		gun.move(this.getX() + this.centerX - gun.getCenterX(), this.getY()
				+ this.centerY - gun.getCenterY());		
	}
	private void moveOrderGun() {		
		orderGun.move((float)(this.getX()+this.getCenterX() + radius*Math.cos(GeometryUtils.degToRad(angle)-Math.PI/2))-orderGun.getCenterX(), (float)(this.getY()+this.getCenterY() + radius*Math.sin(GeometryUtils.degToRad(angle)-Math.PI/2))-orderGun.getCenterY());
	}
	public void moveOrderGun(float x, float y) {
		
		angle=(float)(GeometryUtils.getAngleDeg(x, y, getX()+getCenterX(), getY()+getCenterY()));		
		
				
		moveOrderGun();		
		rotateOrderGun(angle);	
	}
	public BitmapObject getGunOrder() { 
		return orderGun;
	}
	public MathVector getOrderCoords() {
		return new MathVector(this.getX() + this.centerX,this.getY() + this.centerY);
	}

}
