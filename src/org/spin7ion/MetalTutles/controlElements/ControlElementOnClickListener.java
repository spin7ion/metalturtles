package org.spin7ion.MetalTutles.controlElements;

public interface ControlElementOnClickListener{
	public void onControlElementClick(ControlElementEvent event);
}
