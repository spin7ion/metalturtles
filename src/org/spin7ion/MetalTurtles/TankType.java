package org.spin7ion.MetalTurtles;

public class TankType {
	private float maxMoveDistance;
	private float maxFireDistance;
	
	private float gunTurnSpeed;
	private float bodyTurnSpeed;
	private float moveSpeed;
	private long reloadTime;
	private int damage;
	
	private int hp;
	
	private int armorFront;
	private int armorBack;
	private int armorSide;
	
	
	public TankType(float maxMoveDistance, float maxFireDistance,
			float gunTurnSpeed, float bodyTurnSpeed, float moveSpeed, long reloadTime, int hp,
			int armorFront, int armorBack, int armorSide,int damage) {
		
		this.maxMoveDistance = maxMoveDistance;
		this.maxFireDistance = maxFireDistance;
		this.gunTurnSpeed = gunTurnSpeed;
		this.bodyTurnSpeed = bodyTurnSpeed;
		this.moveSpeed = moveSpeed;
		this.reloadTime=reloadTime;
		this.hp = hp;
		this.armorFront = armorFront;
		this.armorBack = armorBack;
		this.armorSide = armorSide;
		this.damage=damage;
	}
	public float getMaxMoveDistance() {
		return maxMoveDistance;
	}
	public float getMaxFireDistance() {
		return maxFireDistance;
	}
	public float getGunTurnSpeed() {
		return gunTurnSpeed;
	}
	public float getBodyTurnSpeed() {
		return bodyTurnSpeed;
	}
	public float getMoveSpeed() {
		return moveSpeed;
	}
	public long getReloadTime() {
		return reloadTime;
	}
	public int getDamage() {
		return damage;
	}
	public int getHp() {
		return hp;
	}
	public int getArmorFront() {
		return armorFront;
	}
	public int getArmorBack() {
		return armorBack;
	}
	public int getArmorSide() {
		return armorSide;
	}
	
	
}
