package org.spin7ion.MetalTurtles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.spin7ion.MetalTurtles.physics.Clip;
import org.spin7ion.MetalTurtles.physics.PhysicsRectange;
import org.spin7ion.Utils.GeometryUtils;
import org.spin7ion.Utils.MathVector;
import org.spin7ion.Utils.Ray;
import org.spin7ion.Utils.Segment;
import org.spin7ion.MetalTurtles.R;
import org.spin7ion.MetalTurtles.controlElements.ControlElement;
import org.spin7ion.MetalTurtles.controlElements.ControlElementEvent;
import org.spin7ion.MetalTurtles.controlElements.ControlElementOnClickListener;
import org.spin7ion.MetalTurtles.controlElements.ReloadGauge;
import org.spin7ion.MetalTurtles.graphics.*;
import org.xmlpull.v1.XmlPullParserException;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.SlidingDrawer;

public class GameContainer implements ControlElementOnClickListener,
		OnTouchListener, OnLongClickListener {

	private GameThread mGameThread;

	private Camera mCamera;
	private DisplayMetrics mMetrics;
	private Resources mResources;
	private List<Tank> tankList = new ArrayList<Tank>();

	private ControlElement zoomIn;
	private ControlElement zoomOut;
	private ControlElement fireButton;
	
	
	private Tank selectedTank;

	private boolean movingMap = false;
	private boolean movingOrder = false;
	private boolean movingGunOrder = false;
	private boolean freezeControls = false;
	private boolean makingTurn = true;
	
	private long lastUpdateTime;
	
	private Map mMap;
	private EffectHolder effects;
	
	long frametime=0;
	public static final long frametimeLimit=60;
	
	String tmpString="";
	
	/**
	 * last touch coordinates
	 */
	float wasX=-1;
	float wasY=-1;

	MathVector penVect;

	public GameContainer(Resources resources, DisplayMetrics metrics) {
		mMetrics = metrics;
		mResources = resources;
		mCamera = new Camera(mMetrics);

		zoomIn = new ControlElement(BitmapFactory.decodeResource(mResources,
				R.drawable.zoomin), 2, metrics.heightPixels
				- BitmapFactory.decodeResource(mResources, R.drawable.zoomin)
						.getHeight() - 2, mMetrics);
		zoomOut = new ControlElement(BitmapFactory.decodeResource(mResources,
				R.drawable.zoomout), zoomIn.getX() + zoomIn.getWidth()
				* 1.3f, zoomIn.getY(), mMetrics);
	
	
		fireButton=new ControlElement(BitmapFactory.decodeResource(mResources,
				R.drawable.fire), 2, 2, mMetrics);
		

		zoomIn.setControlElementOnClickListener(this);
		zoomOut.setControlElementOnClickListener(this);
		fireButton.setControlElementOnClickListener(this);
		
		
		try {
			mMap = new Map(mCamera, resources, mMetrics);
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		effects=new EffectHolder(mCamera,resources, mMetrics);

		createTiger(150, 200, 0);
		createTiger(150, -150, 180);
		allowMakingTurn();
		lastUpdateTime=System.currentTimeMillis();
		mGameThread = new GameThread(this);
		mGameThread.setRunning(true);
		mGameThread.start();
		Log.w("Game", "Started");
	}

	/**
	 * Draws all objects on view
	 * 
	 * @param canvas
	 *            - canvas to draw objects
	 */
	public void drawObjects(Canvas canvas) {
		mCamera.uncache();
		mMap.draw(Map.PRIORITY_GRASS, canvas);
		effects.draw(EffectHolder.PRIORITY_TRACES,canvas);
		synchronized (tankList) {
			Iterator<Tank> iterator = tankList.iterator();
			while (iterator.hasNext()) {
				Tank tmpTank = iterator.next();
				tmpTank.draw(canvas, mCamera, mMetrics);
			}
		}
		mMap.draw(Map.PRIORITY_HOUSES, canvas);
		mMap.draw(Map.PRIORITY_TREES, canvas);
		mMap.draw(Map.PRIORITY_DEBUG_CLIPS, canvas);
		effects.draw(EffectHolder.PRIORITY_OTHER,canvas);
		synchronized (tankList) {
			Iterator<Tank> iterator = tankList.iterator();
			while (iterator.hasNext()) {
				Tank tmpTank = iterator.next();
				tmpTank.drawOrderIfNeeden(canvas, mCamera, mMetrics);
			}
		}
		effects.draw(EffectHolder.PRIORITY_CLOUDS,canvas);
		drawInterface(canvas);
		if (penVect != null) {
			Paint mPaint = new Paint();
			mPaint.setColor(0xFFFF0000);
			canvas.drawText(penVect.toString(), 0, 15, mPaint);
		}
		//drawFPS(canvas);
		drawTest(canvas);
	}
	
	private void drawTest(Canvas canvas) {
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		canvas.drawText(tmpString, mMetrics.widthPixels-100, 15, mPaint);
	}

	public void drawFPS(Canvas canvas){
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		if(frametime!=0){
			canvas.drawText("FPS:"+String.valueOf(1000/frametime), mMetrics.widthPixels-10, 15, mPaint);
		}
	}
	
	public List<Tank> getTanks() {
		return this.tankList;
	}

	/**
	 * @return - if tanks and thier controls freezed
	 */
	public boolean isFreezed() {
		return freezeControls;
	}

	/**
	 * freeze tanks and controls
	 */
	public void freeze() {
		freezeControls = true;
	}

	/**
	 * unfreeze tanks and controls
	 */
	public void unfreeze() {
		freezeControls = false;
	}

	public boolean isUserMakingTurn() {
		return makingTurn;
	}

	public void allowMakingTurn() {
		this.makingTurn = true;
		unfreeze();
	}

	public void disallowMakingTurn() {
		freeze();
		this.makingTurn = false;
	}

	public void makeTurn() {
		disallowMakingTurn();
		synchronized (tankList) {
			Iterator<Tank> iterator = tankList.iterator();
			while (iterator.hasNext()) {
				Tank tmpTank = iterator.next();
				tmpTank.getOrder().startTurn();
				tmpTank.hideOrder();
			}
		}
	}

	public void endMakingTurn() {
		allowMakingTurn();
		synchronized (tankList) {
			Iterator<Tank> iterator = tankList.iterator();
			while (iterator.hasNext()) {
				Tank tmpTank = iterator.next();
				tmpTank.getOrder().finishTurn();
				tmpTank.showOrder();
			}
		}
	}

	private void drawInterface(Canvas canvas) {
		zoomIn.draw(canvas);
		zoomOut.draw(canvas);
		// makeTurn.draw(canvas);
		fireButton.draw(canvas);
	}

	public void onControlElementClick(ControlElementEvent event) {
		if (event.getControlElement() == zoomIn) {
			mCamera.zoomIn(0.1f);
		} else if (event.getControlElement() == zoomOut) {
			mCamera.zoomOut(0.1f);
		}/*
		 * else if (event.getControlElement() == makeTurn) { makeTurn(); }
		 */
		else if (event.getControlElement() == fireButton && selectedTank!=null) { 
			selectedTank.orderFire(); 
		}
		
	}

	public boolean onLongClick(View view) {
		return false;
	}

	public boolean onTouch(View view, MotionEvent event) {
		float touchX=event.getX();
		float touchY=event.getY();
		if(wasX==-1){ wasX=touchX; }
		if(wasY==-1){ wasY=touchY; }
		if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_DOWN) {

			boolean clickedOnElement = false;
			movingOrder = false;
			movingGunOrder = false;

			clickedOnElement = 
					zoomIn.checkClick((int) touchX,
					(int) touchY) ||  
					zoomOut.checkClick((int) touchX,
					(int) touchY) || 
					fireButton.checkClick((int) touchX,
					(int) touchY);
			/*
			 * clickedOnElement = makeTurn.checkClick((int) touchX, (int)
			 * touchY);
			 */
			
			if (!isFreezed()) {
				synchronized (tankList) {
					Iterator<Tank> iterator = tankList.iterator();
					while (iterator.hasNext()) {
						Tank tmpTank = iterator.next();
						if (tmpTank.isOrdershown()) {
							// check tank order click
							if (mCamera.isClicked(touchX, touchY,
									tmpTank.getOrder())) {
								clickedOnElement = true;
								movingOrder = true;
								selectedTank = tmpTank;
								// check gun order click
							} else if (mCamera.isClicked(touchX, touchY, tmpTank.getOrder().getGunOrder())) {
								clickedOnElement = true;
								movingGunOrder = true;
								selectedTank = tmpTank;
								// check fire click
							} 
						} else if (tmpTank.getPhysicsObject().isCollides(
								mCamera.getRelativeScreenX(touchX),
								mCamera.getRelativeScreenY(touchY))) {
							clickedOnElement = true;
							// tmpTank.showOrder();
							selectedTank = tmpTank;
						}

					}
				}
			}
			if (!clickedOnElement) {
				movingMap = true;
			}
		} else if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
			movingMap = false;
		} else if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_MOVE) {
			if (movingMap) {
				
			
					//float wasX=event.getHistoricalX(event.getHistorySize() - 1);
					//float wasY=event.getHistoricalY(event.getHistorySize() - 1);
				
					mCamera.moveCache(
						(touchX - wasX) / mCamera.getZoom(),
						(touchY - wasY) / mCamera.getZoom());
				
			} else if (movingOrder) {
				selectedTank.moveOrder(
						mCamera.getRelativeScreenX(touchX),
						mCamera.getRelativeScreenY(touchY));
			} else if (movingGunOrder) {
				selectedTank.moveGunOrder(
						mCamera.getRelativeScreenX(touchX),
						mCamera.getRelativeScreenY(touchY));
			}

		}		
		wasX=touchX;
		wasY=touchY;
		
		return true;
	}

	private void createTiger(float x, float y, float a) {
		Tank tmpTank = TankFactory.createTank(TankFactory.TANK_TYPE_TIGER,
				this.mResources, this.mMetrics);
		tmpTank.addEffectHolder(this.effects);
		tmpTank.move(x, y);
		tmpTank.getOrder().move(x, y);
		tmpTank.setAngle(a);
		tmpTank.setGunAngle(a);
		tmpTank.initOrder();
		tmpTank.showOrder();
		tankList.add(tmpTank);		
	}
	
	
	
	public long update() {
		synchronized (tankList) {

			
			Iterator<Tank> iterator = tankList.iterator();
			while (iterator.hasNext()) {
				Tank tmpTank = iterator.next();
				// turn gun anyway
				tmpTank.turnGun();
				
				tmpTank.calculateNextPosition();
				// check for collisions
				List<House> houses = mMap.getHouses();
				
				MathVector collisionPoint=null;
				synchronized (houses) {
					Iterator<House> houseIterator = houses.iterator();
					while (houseIterator.hasNext()) {
						House house = houseIterator.next();
						if (((PhysicsRectange) house.getPhysicsObject())
								.isCollides(tmpTank.getNextPhysicsRectange())
								|| tmpTank.getNextPhysicsRectange().isCollides(
										(PhysicsRectange) house
												.getPhysicsObject())) {
							tmpTank.setNextPosition(tmpTank.getCenterCoords());
							tmpTank.setNextAngle(tmpTank.getAngle());
						}
						if(tmpTank.needFire()){
							Ray trace=tmpTank.getFireRay();
							
							
							Segment[] edges=((PhysicsRectange)house.getPhysicsObject()).getEdges();
							for (Segment testSegment : edges) {
								MathVector testCollision=testSegment.getCollision(trace);
								if(collisionPoint==null || (testCollision!=null && testCollision.sub(tmpTank.getCenterCoords()).isLessThan(collisionPoint.sub(tmpTank.getCenterCoords())))){
									collisionPoint=testCollision;
								}
							}
						}
					}
				}
				List<Clip> clips = mMap.getClips();
				synchronized (clips) {
					Iterator<Clip> clipIterator = clips.iterator();
					while (clipIterator.hasNext()) {
						Clip clip = clipIterator.next();
						if (((PhysicsRectange) clip.getPhysicsObject())
								.isCollides(tmpTank.getNextPhysicsRectange())
								|| tmpTank.getNextPhysicsRectange().isCollides(
										(PhysicsRectange) clip
												.getPhysicsObject())) {
							tmpTank.setNextPosition(tmpTank.getCenterCoords());
							tmpTank.setNextAngle(tmpTank.getAngle());
						}
						if(tmpTank.needFire() && !clip.isCanFireThrought()){
							Ray trace=tmpTank.getFireRay();
							
							
							Segment[] edges=((PhysicsRectange)clip.getPhysicsObject()).getEdges();
							for (Segment testSegment : edges) {
								MathVector testCollision=testSegment.getCollision(trace);
								if(collisionPoint==null || (testCollision!=null && testCollision.sub(tmpTank.getCenterCoords()).isLessThan(collisionPoint.sub(tmpTank.getCenterCoords())))){
									collisionPoint=testCollision;
								}
							}
						}
					}
				}

				for (int i = 0; i < tankList.size(); i++) {
					Tank testTank = tankList.get(i);
					if (testTank != tmpTank) {
						if (((PhysicsRectange) testTank.getPhysicsObject())
								.isCollides(tmpTank.getNextPhysicsRectange())
								|| tmpTank.getNextPhysicsRectange().isCollides(
										(PhysicsRectange) testTank
												.getPhysicsObject())) {
							tmpTank.setNextPosition(tmpTank.getCenterCoords());
							tmpTank.setNextAngle(tmpTank.getAngle());
						}
						if(tmpTank.needFire()){
							Ray trace=tmpTank.getFireRay();
							
							
							Segment[] edges=((PhysicsRectange)testTank.getPhysicsObject()).getEdges();
							
							for (int s=0;s<edges.length;s++) {
								Segment testSegment=edges[s];
								
								MathVector testCollision=testSegment.getCollision(trace);
								if(testCollision!=null){
										
									if(collisionPoint==null || (testCollision!=null && testCollision.sub(tmpTank.getCenterCoords()).isLessThan(collisionPoint.sub(tmpTank.getCenterCoords())))){
										collisionPoint=testCollision;
										testTank.getNextOrder().getHpGauge().setCurrentValue(testTank.hit(tmpTank.getTankType().getDamage(), s, (float) trace.getAngleBetween(testSegment)));
									}
								}
							}
						}
					}
				}
				if(tmpTank.needFire()){
					if(collisionPoint==null){
						collisionPoint=tmpTank.getCenterCoords().add(tmpTank.getGunDirection().mult(5000));
					}
					effects.add(new Trace(tmpTank.getCenterCoords(),collisionPoint,mMetrics));
					effects.addExplosion(collisionPoint.getX(),collisionPoint.getY());	
					
					tmpTank.untriggerFire();
				}
				tmpTank.applyNextPosition();
			}

		}
		mMap.update();
		
		long wait=frametimeLimit-(System.currentTimeMillis()-lastUpdateTime);
		if(wait<0)wait=10;
//			wait(wait);
		frametime=System.currentTimeMillis()-lastUpdateTime;
		lastUpdateTime=System.currentTimeMillis();
		return wait;
	}
	
}
