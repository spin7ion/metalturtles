package org.spin7ion.MetalTurtles.physics;

import org.spin7ion.MetalTurtles.graphics.Camera;

import android.graphics.Canvas;

public abstract class PhysicsObject implements Cloneable {
	public abstract boolean isCollides(PhysicsObject test);

	public abstract boolean isCollides(float x, float y);

	public abstract void move(float x, float y);

	public abstract void setSpeed(float vx, float vy);

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	public void draw(Canvas myCanvas, Camera myCamera){}
}
