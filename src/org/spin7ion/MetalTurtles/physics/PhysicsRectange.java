package org.spin7ion.MetalTurtles.physics;

import org.spin7ion.MetalTurtles.graphics.Camera;
import org.spin7ion.Utils.GeometryUtils;
import org.spin7ion.Utils.MathVector;
import org.spin7ion.Utils.Segment;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

public class PhysicsRectange extends PhysicsObject {

	private float centerX, centerY, width, height, angle;

	public float getAngle() {
		return angle;
	}

	public void setAngleRad(float angle) {
		this.angle = angle;
	}

	public void setAngleDeg(float angle) {
		this.angle = GeometryUtils.degToRad(angle);
	}

	public MathVector getCenterVector() {
		return new MathVector(centerX, centerY);
	}

	public float getCenterX() {
		return centerX;
	}

	public void setCenterX(float centerX) {
		this.centerX = centerX;
	}

	public float getCenterY() {
		return centerY;
	}

	public void setCenterY(float centerY) {
		this.centerY = centerY;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public PhysicsRectange(float centerX, float centerY, float width,
			float height) {
		super();
		this.centerX = centerX;
		this.centerY = centerY;
		this.width = width;
		this.height = height;
		this.angle = 0.f;
	}	

	@Override
	public boolean isCollides(PhysicsObject test) {
		// TODO Auto-generated method stub
		return false;
	}

	public float getPenetrationDepth(float x, float y){
		MathVector point=new MathVector(x-centerX, y-centerY).rotate(-angle);
		
		MathVector heightV=new MathVector(0,-height/2.f).rotate(angle);
		MathVector widthV=new MathVector(-width/2.f,0).rotate(angle);
		
		MathVector rightDownCorner=heightV.sub(widthV).rotate(-angle);
		MathVector leftUpCorner=heightV.neg().add(widthV).rotate(-angle);
		
		
		return (float) Math.hypot(
				Math.min(
						Math.abs( leftUpCorner.getX()-point.getX() ), 
						Math.abs( rightDownCorner.getX()-point.getX() )
				),
				Math.min(
						Math.abs( leftUpCorner.getY()-point.getY() ),
						Math.abs( rightDownCorner.getY()-point.getY() )
				) );
	}
	public float getPenetrationDepth(PhysicsRectange test){
		MathVector heightV = new MathVector(0, -height / 2.f).rotate(angle);
		MathVector widthV = new MathVector(-width / 2.f, 0).rotate(angle);

		MathVector aCorner = new MathVector(centerX, centerY).add(heightV
				.sub(widthV));
		MathVector bCorner = new MathVector(centerX, centerY).add(heightV.neg()
				.add(widthV));
		MathVector cCorner = new MathVector(centerX, centerY).add(heightV
				.add(widthV));
		MathVector dCorner = new MathVector(centerX, centerY).add(heightV.neg()
				.sub(widthV));

		return Math.min(
				Math.min(test.getPenetrationDepth(aCorner.getX(), aCorner.getY()), test.getPenetrationDepth(bCorner.getX(), bCorner.getY())),
				Math.min(test.getPenetrationDepth(cCorner.getX(), cCorner.getY()), test.getPenetrationDepth(dCorner.getX(), dCorner.getY())));

	}
	@Override
	public boolean isCollides(float x, float y) {

		MathVector point = new MathVector(x - centerX, y - centerY)
				.rotate(angle);

		MathVector heightV = new MathVector(0, -height / 2.f).rotate(angle);
		MathVector widthV = new MathVector(-width / 2.f, 0).rotate(angle);

		MathVector rightDownCorner = heightV.sub(widthV).rotate(angle);
		MathVector leftUpCorner = heightV.neg().add(widthV).rotate(angle);

		return (leftUpCorner.getX() < point.getX() && rightDownCorner.getX() > point
				.getX())
				&& (leftUpCorner.getY() > point.getY() && rightDownCorner
						.getY() < point.getY());
	}

	public boolean isCollides(PhysicsRectange test) {

		MathVector heightV = new MathVector(0, -height / 2.f).rotate(angle);
		MathVector widthV = new MathVector(-width / 2.f, 0).rotate(angle);

		MathVector aCorner = new MathVector(centerX, centerY).add(heightV
				.sub(widthV));
		MathVector bCorner = new MathVector(centerX, centerY).add(heightV.neg()
				.add(widthV));
		MathVector cCorner = new MathVector(centerX, centerY).add(heightV
				.add(widthV));
		MathVector dCorner = new MathVector(centerX, centerY).add(heightV.neg()
				.sub(widthV));

		return test.isCollides(aCorner.getX(), aCorner.getY())
				|| test.isCollides(bCorner.getX(), bCorner.getY())
				|| test.isCollides(cCorner.getX(), cCorner.getY())
				|| test.isCollides(dCorner.getX(), dCorner.getY());

	}

	public MathVector getPenetrationVector(MathVector point) {

		point = point.sub(new MathVector(centerX, centerY)).rotate(angle);

		MathVector heightV = new MathVector(0, -height / 2.f).rotate(angle);
		MathVector widthV = new MathVector(-width / 2.f, 0).rotate(angle);

		MathVector rightDownCorner = heightV.sub(widthV).rotate(angle);
		MathVector leftUpCorner = heightV.neg().add(widthV).rotate(angle);

		return new MathVector(GeometryUtils.minAbs(
				leftUpCorner.getX() - point.getX(), rightDownCorner.getX()
						- point.getX()), GeometryUtils.minAbs(
				leftUpCorner.getY() - point.getY(), rightDownCorner.getY()
						- point.getY())).rotate(angle);

		/*
		 * return (leftUpCorner.getX()<point.getX() &&
		 * rightDownCorner.getX()>point.getX())&&
		 * (leftUpCorner.getY()>point.getY() &&
		 * rightDownCorner.getY()<point.getY());
		 */
	}

	public MathVector getPenetrationVector(PhysicsRectange test) {

		MathVector heightV = new MathVector(0, -height / 2.f).rotate(angle);
		MathVector widthV = new MathVector(-width / 2.f, 0).rotate(angle);

		MathVector aCorner = new MathVector(centerX, centerY).add(heightV
				.sub(widthV));
		MathVector bCorner = new MathVector(centerX, centerY).add(heightV.neg()
				.add(widthV));
		MathVector cCorner = new MathVector(centerX, centerY).add(heightV
				.add(widthV));
		MathVector dCorner = new MathVector(centerX, centerY).add(heightV.neg()
				.sub(widthV));

		MathVector a = GeometryUtils
				.min(GeometryUtils.min(getPenetrationVector(aCorner),
						getPenetrationVector(bCorner)),
						GeometryUtils.min(getPenetrationVector(cCorner),
								getPenetrationVector(dCorner))).add(heightV)
				.add(widthV).rotate(-angle);
		Log.w("Penetration Vector", a.toString());
		return a;
	}

	public MathVector[] getCorners() {
		MathVector heightV = new MathVector(0, -height / 2.f).rotate(angle);
		MathVector widthV = new MathVector(-width / 2.f, 0).rotate(angle);

		MathVector[] a = {
				new MathVector(centerX, centerY).add(heightV.sub(widthV)),
				new MathVector(centerX, centerY).add(heightV.add(widthV)),
				new MathVector(centerX, centerY).add(heightV.neg().add(widthV)),				
				new MathVector(centerX, centerY).add(heightV.neg().sub(widthV)) };
		return a;
	}
	
	public Segment[] getEdges() {
		MathVector []corners=getCorners();
		
		Segment[] a = {new Segment(corners[0], corners[1]),new Segment(corners[1], corners[2]),new Segment(corners[2], corners[3]),new Segment(corners[3], corners[0])};
		return a;
	}
	@Override
	public void draw(Canvas myCanvas, Camera myCamera){
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		Segment[] a = this.getEdges();

		myCanvas.drawLine(myCamera.getDotScreenX(a[0].getX()),
				myCamera.getDotScreenY(a[0].getY()),
				myCamera.getDotScreenX(a[0].getEnd().getX()),
				myCamera.getDotScreenY(a[0].getEnd().getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[1].getX()),
				myCamera.getDotScreenY(a[1].getY()),
				myCamera.getDotScreenX(a[1].getEnd().getX()),
				myCamera.getDotScreenY(a[1].getEnd().getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[2].getX()),
				myCamera.getDotScreenY(a[2].getY()),
				myCamera.getDotScreenX(a[2].getEnd().getX()),
				myCamera.getDotScreenY(a[2].getEnd().getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[3].getX()),
				myCamera.getDotScreenY(a[3].getY()),
				myCamera.getDotScreenX(a[3].getEnd().getX()),
				myCamera.getDotScreenY(a[3].getEnd().getY()), mPaint);
	}
	
	public void move(float x, float y) {
		centerX = x;
		centerY = y;
	}

	@Override
	public void setSpeed(float vx, float vy) {
		// TODO Auto-generated method stub

	}
}
