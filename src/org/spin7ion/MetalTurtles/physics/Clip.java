package org.spin7ion.MetalTurtles.physics;

public class Clip {
	private PhysicsObject mPhysicsObject=null;
	private boolean canFireThrought=false;
	
	public Clip(PhysicsObject mPhysicsObject, boolean canFireThrought) {
		super();
		this.mPhysicsObject = mPhysicsObject;
		this.canFireThrought = canFireThrought;
	}
	
	public PhysicsObject getPhysicsObject() {
		return mPhysicsObject;
	}
	public void setPhysicsObject(PhysicsObject mPhysicsObject) {
		this.mPhysicsObject = mPhysicsObject;
	}
	public boolean isCanFireThrought() {
		return canFireThrought;
	}
	public void setCanFireThrought(boolean canFireThrought) {
		this.canFireThrought = canFireThrought;
	}
}
