package org.spin7ion.MetalTurtles.physics;

import org.spin7ion.MetalTurtles.graphics.Camera;

import android.graphics.Canvas;
import android.graphics.Paint;

public class PhysicsCircle extends PhysicsObject {
	private float circleX,circleY,radius;
	
	public float getCircleX() {
		return circleX;
	}

	public void setCircleX(float circleX) {
		this.circleX = circleX;
	}

	public float getCircleY() {
		return circleY;
	}

	public void setCircleY(float circleY) {
		this.circleY = circleY;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public PhysicsCircle(float circleX,float circleY,float radius){
		this.circleX=circleX;
		this.circleY=circleY;
		this.radius=radius;
	}
	
	@Override
	public boolean isCollides(PhysicsObject test) {
		
		return false;
	}

	@Override
	public boolean isCollides(float x, float y) {		
		return Math.pow(x-circleX, 2)+Math.pow(y-circleY, 2)<Math.pow(radius, 2);
	}

	@Override
	public void move(float x, float y) {
		circleX=x;
		circleY=y;		
	}

	@Override
	public void setSpeed(float vx, float vy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void draw(Canvas myCanvas, Camera myCamera){
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		myCanvas.drawCircle(myCamera.getDotScreenX(circleX), myCamera.getDotScreenY(circleY), radius, mPaint);
	}

}
