package org.spin7ion.MetalTurtles.physics;

import java.util.Vector;

import org.spin7ion.Utils.MathVector;

public class CoordsHistory {
	public class HistoryPoint{
		private MathVector coords;
		private float angle;
		private float gunAngle;
		private float time;
		
		public HistoryPoint(MathVector newCoords,float newAngle,float newGunAngle, float newTime){
			coords=newCoords;
			angle=newAngle;
			gunAngle=newGunAngle;
			time=newTime;
		}
		
		public MathVector getCoords() {
			return coords;
		}
		public void setCoords(MathVector coords) {
			this.coords = coords;
		}
		public float getTime() {
			return time;
		}
		public void setTime(float time) {
			this.time = time;
		}
		public float getAngle() {
			return angle;
		}
		public void setAngle(float angle) {
			this.angle = angle;
		}

		public float getGunAngle() {
			return gunAngle;
		}

		public void setGunAngle(float gunAngle) {
			this.gunAngle = gunAngle;
		}
	}
	Vector<HistoryPoint> history;
		
	
	public CoordsHistory(){
		history=new Vector<CoordsHistory.HistoryPoint>();
	}
	
	public void clear(){
		history.clear();
	}
	public HistoryPoint getHistoryPoint(int id){
		return history.get(id);
	}
	public void addPoint(MathVector newCoords,float newAngle,float newGunAngle, float newTime){
		history.insertElementAt(new HistoryPoint(newCoords, newAngle,newGunAngle, newTime), 0);
	}
}
