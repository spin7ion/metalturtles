package org.spin7ion.MetalTurtles.graphics;

import org.spin7ion.Utils.MathVector;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.util.DisplayMetrics;

public class Trace extends GraphicsEffect{
	MathVector end;
	MathVector dir;
	private static int maxlifetime=350; 
	public Trace(float x1, float y1,float x2, float y2,DisplayMetrics metrics){
		super(x1,y1,metrics);
		this.lifeTime=maxlifetime;
		end=new MathVector(x2, y2);
		dir=end.sub(new MathVector(x1, y1)).norm().mult(5).ort();
	}
	public Trace(MathVector start,MathVector end,DisplayMetrics metrics){
		super(start.getX(),start.getY(),metrics);
		this.lifeTime=maxlifetime;
		this.end=end;
		dir=end.sub(start).norm().mult(5).ort();
	}
	@Override
	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		if(lifeTime>0){
		 	Paint p = new Paint();
		 	p.setStrokeWidth(6);
		 	p.setAlpha((int) this.lifeTime*255/maxlifetime);		 
		    // start at 0,0 and go to 0,max to use a vertical
		    // gradient the full height of the screen.
		    p.setShader(new LinearGradient(0,0,dir.getX(),dir.getY(), Color.RED, Color.YELLOW, android.graphics.Shader.TileMode.MIRROR));		    
		    myCanvas.drawLine(myCamera.getDotScreenX(x), myCamera.getDotScreenY(y), myCamera.getDotScreenX(end.getX()), myCamera.getDotScreenY(end.getY()), p);
			
		}

	}

}
