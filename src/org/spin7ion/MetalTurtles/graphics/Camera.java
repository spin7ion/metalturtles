package org.spin7ion.MetalTurtles.graphics;

import org.spin7ion.MetalTurtles.GameObject;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

public class Camera {
	private float x, y;
	private float cacheX, cacheY;
	private DisplayMetrics metrics;
	private Bitmap filter = null;
	private long filterend;

	private float zoom=1.0f;
	
	public Camera(DisplayMetrics nmetrics) {
		this.metrics = nmetrics;
		x = 0;
		y = 0;
	}

	public float getCameraX() {
		return x;
	}

	public float getCameraY() {
		return y;
	}

	public void followObject(GameObject target) {
		this.x = this.metrics.widthPixels / 2 - target.getX();
		this.y = this.metrics.heightPixels / 2 - target.getY();
	}

	public void followCords(float x, float y) {
		this.x = this.metrics.widthPixels / 2 - x;
		this.y = this.metrics.heightPixels / 2 - y;
	}

	public float getObjectScreenX(GraphicObject target) {
		return (this.x + target.getX());
	}

	public float getObjectScreenY(GraphicObject target) {
		return (this.y + target.getY());
	}
	
	public float getObjectCenterScreenX(GraphicObject target) {
		return (this.x + target.getX()+target.getCenterX());
	}

	public float getObjectCenterScreenY(GraphicObject target) {
		return (this.y + target.getY()+target.getCenterY());
	}
	
	public float getObjectScreenWidth(BitmapObject target) {
		return target.getWidth()*zoom;
	}

	public float getObjectScreenHeight(BitmapObject target) {
		return target.getHeight()*zoom;
	}

	public float getRelativeScreenX(float x) {
		return (x/zoom - this.x);
	}

	public float getRelativeScreenY(float y) {
		return (y/zoom - this.y);
	}
	
	public float getDotScreenX(float x) {
		return (x + this.x)*zoom;
	}

	public float getDotScreenY(float y) {
		return (y + this.y)*zoom;
	}

	public boolean needFilter() {
		if (filter != null && System.currentTimeMillis() > filterend) {
			filter = null;
			return false;
		} else if (filter == null) {
			return false;
		}
		return true;
	}

	public Bitmap getFilter() {
		return filter;
	}

	public Bitmap setFilter() {
		return filter;
	}

	public void drawFilter(Canvas canvas) {
		if (this.needFilter()) {
			canvas.drawBitmap(this.getFilter(), 0, 0, null);
		}
	}

	public boolean isObjectVisible(BitmapObject graphicsObject) {
		float x = getObjectScreenX(graphicsObject)*zoom;
		float y = getObjectScreenY(graphicsObject)*zoom;
		if (x > -graphicsObject.getWidth()*zoom && x < metrics.widthPixels) {
			if (y > -graphicsObject.getHeight()*zoom && y < metrics.heightPixels) {
				return true;
			}
		}
		return false;
	}
	public boolean isClicked(float cx,float cy,BitmapObject graphicsObject) {				
		if (graphicsObject.getX()<getRelativeScreenX(cx) && getRelativeScreenX(cx)<graphicsObject.getX()+graphicsObject.getWidth() && 
				graphicsObject.getY()<getRelativeScreenY(cy) && getRelativeScreenY(cy)<graphicsObject.getY()+graphicsObject.getHeight()) {			
				return true;			
		}
		return false;
	}

	public float getZoom() {
		return zoom;
	}
	public void zoomIn(float amount) {
		zoom+=amount;
	}
	public void zoomOut(float amount) {
		zoom-=amount;
	}

	public void move(float x, float y) {		
		this.x+=x;
		this.y+=y;
		this.cacheX=x;
		this.cacheY=y;
	}

	public void moveCache(float x, float y) {
		this.cacheX+=x;
		this.cacheY+=y;
	}
	public void uncache(){
		this.x=this.cacheX;
		this.y=this.cacheY;
	}
}
