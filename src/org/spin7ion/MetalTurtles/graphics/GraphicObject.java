package org.spin7ion.MetalTurtles.graphics;

import org.spin7ion.Utils.MathVector;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

public abstract class GraphicObject {
	protected float x, y;
	protected float centerX, centerY;
	protected float angle;
	protected boolean absolutePosition = false;
	protected DisplayMetrics metrics;
	
	public GraphicObject(float x2, float y2,DisplayMetrics metrics) {
		this.x = x2;
		this.y = y2;
		this.angle = 0;
		this.metrics=metrics;
	}

	public GraphicObject(float x, float y, float centerX,
			float centerY,DisplayMetrics metrics) {
		this.x = x;
		this.y = y;
		this.centerX = centerX;
		this.centerY = centerY;
		this.angle = 0;
		this.metrics=metrics;
	}

	public GraphicObject( float x, float y, float centerX,
			float centerY, float angle,DisplayMetrics metrics) {
		this.x = x;
		this.y = y;
		this.centerX = centerX;
		this.centerY = centerY;
		this.angle = angle;
		this.metrics=metrics;
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getCenterX() {
		return centerX;
	}

	public float getCenterY() {
		return centerY;
	}

	public void setAbsolute() {
		absolutePosition = true;
	}

	public void setRelative() {
		absolutePosition = false;
	}
	
	public void move(float x, float y) {
		this.x = x;
		this.y = y;
	}
	public void moveCenter(float x, float y) {
		this.x = x-centerX;
		this.y = y-centerY;
	}
	public void moveCenter(MathVector coords) {
		this.x = coords.getX()-centerX;
		this.y = coords.getY()-centerY;
	}
	public MathVector getCenterCoords() {
		return new MathVector(this.getX() + this.centerX,this.getY() + this.centerY);
	}
	public void rotate(float d) {
		this.angle += d;
	}
	
	public void setAngle(float d) {
		this.angle=d;
	}
	public float getAngle() {
		return this.angle;
	}
	
	public abstract void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics);
}
