package org.spin7ion.MetalTurtles.graphics;

import android.util.DisplayMetrics;

public abstract class GraphicsEffect extends GraphicObject {

	public GraphicsEffect(float x2, float y2,DisplayMetrics metrics) {
		super(x2, y2,metrics);
	}
	protected long lifeTime;
	protected boolean unlimited=false;
	
	public void update(long a){ lifeTime-=a; }
	public boolean isDead(){ return lifeTime<0; }
	public boolean isAlive(){ return lifeTime>-1; }
	public void setUnlimited(){ unlimited=true; }
	public boolean isUnlimited(){ return unlimited; }	
	
}
