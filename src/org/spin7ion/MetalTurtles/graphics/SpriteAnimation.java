package org.spin7ion.MetalTurtles.graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;

public class SpriteAnimation extends GraphicsEffect {

	private Rect srcRect = new Rect();
	private Rect dstRect = null;
	private int msPerFrame;
	private int cntOfFrames;
	private int currentFrame;

	private Bitmap bitmap;
	private int spriteWidth;
	private int spriteHeight;
	private int spriteRows;
	private int spriteCols;

	public SpriteAnimation(Bitmap bitmap, float x2, float y2,DisplayMetrics metrics) {
		super(x2, y2,metrics);
		this.setBitmap(bitmap);
		
	}

	public void initSpriteData(int rows, int cols, int width, int height,
			int fps, int cntOfFrames) {
		this.spriteRows = rows;
		this.spriteCols = cols;
		this.spriteWidth = width;
		this.spriteHeight = height;
		this.srcRect.top = 0;
		this.srcRect.bottom = spriteHeight;
		this.srcRect.left = 0;
		this.srcRect.right = spriteWidth;
		this.msPerFrame =  fps;
		this.cntOfFrames = cntOfFrames;
		lifeTime=10;
	}

	@Override
	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		dstRect=new Rect( (int) (myCamera.getDotScreenX(getX())-spriteWidth*myCamera.getZoom()/2),
				(int) (myCamera.getDotScreenY(getY())-spriteWidth*myCamera.getZoom()/2),
				 (int) (myCamera.getDotScreenX(getX())+spriteWidth*myCamera.getZoom()/2),
				  (int) (myCamera.getDotScreenY(getY())+spriteHeight*myCamera.getZoom()/2));
		
		myCanvas.drawBitmap(getBitmap(), srcRect, dstRect, null);

	}

	@Override
	public void update(long currentTime) {
		
		if (currentTime > msPerFrame) {
			currentFrame++;

			if (currentFrame >= cntOfFrames) {
				if(isUnlimited()){
					currentFrame = 0;
				}else{
					currentFrame = cntOfFrames;
					lifeTime=-1;
				}
			}
			srcRect.left = currentFrame % spriteCols * spriteWidth;
			srcRect.right = srcRect.left + spriteWidth;

			int rowNum = currentFrame / spriteCols;
			srcRect.top = rowNum * spriteHeight;
			srcRect.bottom = srcRect.top + spriteHeight;
		}

		// srcRect.left = currentFrame * spriteWidth;
		// srcRect.right = srcRect.left + spriteWidth;

		

	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

}
