package org.spin7ion.MetalTurtles.graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;

public class BitmapObject extends GraphicObject{
	protected Bitmap graphics;
	
	public BitmapObject(Bitmap graphics, float x2, float y2,DisplayMetrics metrics) {
		super(x2,y2,metrics);
		this.graphics = graphics;
	}

	public BitmapObject(Bitmap graphics, float x, float y, float centerX,
			float centerY,DisplayMetrics metrics) {
		super(x, y, centerX, centerY,metrics);
		this.graphics = graphics;
	}

	public BitmapObject(Bitmap graphics, float x, float y, float centerX,
			float centerY, float angle,DisplayMetrics metrics) {
		super(x, y, centerX, centerY, angle,metrics);
		this.graphics = graphics;
	}

	public Bitmap getGraph() {
		return graphics;
	}
	
	public void setGraph(Bitmap graphics) {
		this.graphics=graphics;
	}

	public int getWidth() {
		return graphics.getScaledWidth(metrics);
	}

	public int getHeight() {
		return graphics.getScaledHeight(metrics);
	}

	public void draw(Canvas myCanvas) {
		if (this.angle != 0) {
			myCanvas.save();
			myCanvas.rotate(this.angle, this.getX(), this.getY());
			myCanvas.drawBitmap(this.getGraph(), this.getX(), this.getY(), null);
			myCanvas.restore();
		} else {
			myCanvas.drawBitmap(this.getGraph(), this.getX(), this.getY(), null);
		}
	}	
	
	public boolean isPointCollide(float pX, float pY, DisplayMetrics metrics) {		
		if (this.getX()<pX && pX < this.getX()+this.getGraph().getScaledWidth(metrics)
				&& this.getY()<pY && pY < this.getY()+this.getGraph().getScaledHeight(metrics)) {		
			return true;
		}

		return false;
	}

	@Override
	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		if (!absolutePosition && !myCamera.isObjectVisible(this)) {
			return;
		}

		myCanvas.save();
		if (absolutePosition) {
			myCanvas.rotate(this.angle, getX() + getCenterX(), getY() + getCenterY());
			myCanvas.drawBitmap(this.getGraph(), getX(), getY(), null);
		} else {
			Matrix matrix = new Matrix();
			
			matrix.postRotate(this.angle, getCenterX(),getCenterY());
			matrix.postTranslate(myCamera.getObjectScreenX(this), myCamera.getObjectScreenY(this));
			matrix.postScale(myCamera.getZoom(), myCamera.getZoom());
			
			myCanvas.drawBitmap(this.getGraph(),matrix,null);
			
		}				
		myCanvas.restore();
	}

	public void drawCenterPoint(Canvas myCanvas, Camera myCamera,
			DisplayMetrics metrics) {
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		Log.w(String.valueOf(getCenterY()), String.valueOf(myCamera.getZoom()));
		myCanvas.drawCircle(myCamera.getObjectScreenX(this)+getCenterX()*myCamera.getZoom(),
				myCamera.getObjectScreenY(this)+getCenterY()*myCamera.getZoom(), 1, mPaint);
	}	
	
}
