package org.spin7ion.MetalTurtles.graphics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.spin7ion.MetalTurtles.R;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

public class EffectHolder {

	public static final int PRIORITY_TRACES = 0;
	public static final int PRIORITY_OTHER = 1;
	public static final int PRIORITY_CLOUDS = 3;

	private List<GraphicsEffect> otherEffects;
	private List<GraphicsEffect> tracesEffects;

	private DisplayMetrics mMetrics;
	private Camera mCamera;
	
	private long lastUpdateTime;
	private Resources res;

	public EffectHolder(Camera mCamera, Resources res, DisplayMetrics dm) {
		this.mMetrics = dm;
		this.mCamera = mCamera;
		this.res=res;
		otherEffects = new ArrayList<GraphicsEffect>();
		tracesEffects = new ArrayList<GraphicsEffect>();
		lastUpdateTime=System.currentTimeMillis();
	}

	public void add(GraphicsEffect newEffect) {
		if (newEffect instanceof Trace) {
			synchronized (tracesEffects) {
				tracesEffects.add(newEffect);
			}
		} else {
			synchronized (otherEffects) {
				otherEffects.add(newEffect);
			}
		}
	}
	public SpriteAnimation addExplosion(float x, float y){
		SpriteAnimation explosionAni = new SpriteAnimation(BitmapFactory.decodeResource(res,R.drawable.explosion_transparent),x,y,mMetrics);
		explosionAni.initSpriteData(4, 4, explosionAni.getBitmap().getWidth()/4, explosionAni.getBitmap().getHeight()/4, 1, 16);
		this.add(explosionAni);
		return explosionAni;
	}
	public SpriteAnimation addSmoke(float x, float y, boolean unlimited){
		SpriteAnimation smokeAni = new SpriteAnimation(BitmapFactory.decodeResource(res,R.drawable.smoke_big),x+(64*mMetrics.density),y+(64*mMetrics.density),mMetrics);
		smokeAni.initSpriteData(5, 8, (int)(128*mMetrics.density), (int) (128*mMetrics.density), 1, 40);
		if(unlimited){
			smokeAni.setUnlimited();
		}
		this.add(smokeAni);
		return smokeAni;
	}
	public void draw(int priority, Canvas canvas) {
		if (PRIORITY_TRACES == priority) {
			synchronized (otherEffects) {
				Iterator<GraphicsEffect> iterator = otherEffects.iterator();
				while (iterator.hasNext()) {
					GraphicsEffect effect = iterator.next();
					effect.update(System.currentTimeMillis()-lastUpdateTime);
					effect.draw(canvas, mCamera, mMetrics);
					if (effect.isDead() && !effect.isUnlimited()) {						
						iterator.remove();
					}
				}
			}
		} else if (PRIORITY_OTHER == priority) {
			synchronized (tracesEffects) {
				Iterator<GraphicsEffect> iterator = tracesEffects.iterator();
				while (iterator.hasNext()) {
					GraphicsEffect effect = iterator.next();
					effect.update(System.currentTimeMillis()-lastUpdateTime);
					effect.draw(canvas, mCamera, mMetrics);
					if (effect.isDead() && !effect.isUnlimited()) {
						iterator.remove();
					}
				}
			}
		} else if (PRIORITY_CLOUDS == priority) {
			
		}
		
		
		lastUpdateTime=System.currentTimeMillis();
	}
}
