package org.spin7ion.MetalTurtles;

import org.spin7ion.MetalTurtles.graphics.BitmapObject;
import org.spin7ion.MetalTurtles.graphics.Camera;
import org.spin7ion.MetalTurtles.physics.PhysicsObject;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

public class GameObject extends BitmapObject {
	private int width, height;
	protected PhysicsObject myPhysObject;
	DisplayMetrics metrics;
	public final static int noCollision = 0;
	public final static int leftEdge = 1;
	public final static int rightEdge = 2;
	public final static int upEdge = 3;
	public final static int downEdge = 4;
	public final static int leftCol = 5;
	public final static int rightCol = 6;
	public final static int upCol = 7;
	public final static int downCol = 8;

	public GameObject(Bitmap graphics, float x, float y, DisplayMetrics metrics) {
		super(graphics, x, y,metrics);
		setWidth(metrics.widthPixels);
		setHeight(metrics.heightPixels);
		this.metrics = metrics;
	}
	public GameObject(Bitmap graphics, float x, float y,float bodyCenterX, float bodyCenterY, DisplayMetrics metrics) {
		super(graphics, x, y,bodyCenterX, bodyCenterY,metrics);
		setWidth(metrics.widthPixels);
		setHeight(metrics.heightPixels);
		this.metrics = metrics;
	}

	

	public void update() {
		//updatecords();
	}

	public int DoesCollScreen() {
		if (this.x < 0)
			return leftEdge;
		else if (this.x > width - this.getGraph().getScaledWidth(metrics))
			return rightEdge;

		if (this.y < 0)
			return upEdge;
		else if (this.y > height - this.getGraph().getScaledHeight(metrics)*2)
			return downEdge;

		return noCollision;
	}
	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		super.draw(myCanvas,myCamera,metrics);
		if(myPhysObject!=null){
			myPhysObject.draw(myCanvas, myCamera);
		}
	}
	/*public int screenCheck() {
		int scrColl = DoesCollScreen();
		switch (scrColl) {
		case leftEdge:
			if (x < 0) {
				vx = -vx;
			}
			break;
		case rightEdge:
			if (x > 0) {
				vx = -vx;
			}
			break;
		case upEdge:
			if (y < 0) {
				vy = -vy;
			}
			// y=1;
			break;
		case downEdge:
			if (y > 0) {
				vy = -vy;
			}
			break;
		}
		return scrColl;
	}

	private void updatecords() {
		this.x += vx;
		this.y += vy;

	}

	// only check for collision
	public boolean DoesColide(GameObject test) {
		if (Math.abs(x+vx - test.getX()) < (test.getGraph()
				.getScaledWidth(metrics) + this.getGraph().getScaledWidth(
				metrics)) / 2) {
			if (Math.abs(y+vy - test.getY()) < (test.getGraph().getScaledHeight(
					metrics) + this.getGraph().getScaledHeight(metrics)) / 2) {
				return true;
			}
		}
		return false;
	}

	// collision of this for test
	// up if test is BELOW this
	// left if test is on right side of this
	public int advancedCollision(GameObject test) {
		if (DoesColide(test)) {
			int cx = (int) (x+vx-test.x);
			int cy = (int) (y+vy-test.y);
			if(cy>test.getGraph().getScaledHeight(metrics)-this.getGraph().getScaledHeight(metrics)/2)
				return downCol;
			else if(cy<-this.getGraph().getScaledHeight(metrics)/2)
				return upCol;
			else if(cx>test.getGraph().getScaledWidth(metrics)-this.getGraph().getScaledWidth(metrics)/2)
				return rightCol;
			else if(cx<-this.getGraph().getScaledWidth(metrics)/2)
				return leftCol;

		}
		return noCollision;
	}
	public void multiplySpeed(float mylt){
		this.vx=mylt*this.vx;
		this.vy=mylt*this.vy;
	}*/
	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}
	public PhysicsObject getPhysicsObject(){
		return myPhysObject;
	}
	public void setPhysicsObject(PhysicsObject mPhysObj){
		myPhysObject=mPhysObj;
	}
}
