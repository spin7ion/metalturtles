package org.spin7ion.MetalTurtles;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	private ViewThread mThread;
	private GameContainer gContainer;
	private DisplayMetrics mMetrics;
		
	
	public GameView(Context context,DisplayMetrics metrics) {
		super(context);
		getHolder().addCallback(this);
		
		mMetrics=metrics;		
		
		gContainer=new GameContainer(getResources(),mMetrics);		
		
		mThread = new ViewThread(this);
		this.setOnTouchListener(gContainer);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	public void surfaceCreated(SurfaceHolder holder) {
		if (!mThread.isAlive()) {
			mThread = new ViewThread(this);
			mThread.setRunning(true);
			mThread.start();
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mThread.isAlive()) {
			mThread.setRunning(false);
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {	
		canvas.drawColor(Color.BLACK);
		gContainer.drawObjects(canvas);
	}
}
