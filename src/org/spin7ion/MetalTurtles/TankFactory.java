package org.spin7ion.MetalTurtles;

import org.spin7ion.MetalTurtles.R;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

public class TankFactory {
	/**
	 * Tank types
	 */
	public static final int TANK_TYPE_TIGER=1;
	
	/**
	 *  Tanks' guns centers
	 */
	private static final int TANK_GUN_TIGER_X=20;
	private static final int TANK_GUN_TIGER_Y=84;
	/**
	 *  Tanks' bodies centers
	 */
	private static int TANK_BODY_TIGER_X=30;
	private static int TANK_BODY_TIGER_Y=42;
	/**
	 *  Tanks' max distance in turn
	 */
	private static final float TANK_MAX_MOVE_DISTANCE_TIGER=200.f;
	private static final float TANK_MAX_FIRE_DISTANCE_TIGER=400.f;
	private static final float TANK_GUN_TURN_SPEED_TIGER=2.f;
	private static final float TANK_BODY_TURN_SPEED_TIGER=1.f;
	private static final float TANK_MOVE_SPEED_TIGER=2.f;
	private static final long TANK_RELOAD_TIME_TIGER=5000;
	private static final int TANK_DAMAGE_TIGER=200;
	private static final int TANK_HP_TIGER=2000;
	private static final int TANK_ARMOR_FRONT_TIGER=200;
	private static final int TANK_ARMOR_BACK_TIGER=100;
	private static final int TANK_ARMOR_SIDE_TIGER=150;
	
	/**
	 * Creates tank object
	 * @param tankType - type of tank(Tiger, T-34, etc)
	 * @param res - resources from where to load images
	 * @param dm - DisplayMetrics
	 * @return created tank
	 */
	
	public static Tank createTank(int tankType,Resources res,DisplayMetrics dm){
	    if (tankType==TANK_TYPE_TIGER){
	    	TankType tigerType=new TankType(TANK_MAX_MOVE_DISTANCE_TIGER,TANK_MAX_FIRE_DISTANCE_TIGER,TANK_GUN_TURN_SPEED_TIGER,TANK_BODY_TURN_SPEED_TIGER,TANK_MOVE_SPEED_TIGER,TANK_RELOAD_TIME_TIGER,TANK_HP_TIGER,TANK_ARMOR_FRONT_TIGER,TANK_ARMOR_BACK_TIGER,TANK_ARMOR_SIDE_TIGER,TANK_DAMAGE_TIGER);
	    	Bitmap body=BitmapFactory.decodeResource(res, R.drawable.tigerbody);
	    	Bitmap gun=BitmapFactory.decodeResource(res, R.drawable.tigergun);
	    	return new Tank(body,gun,res,0,0,TANK_BODY_TIGER_X*dm.density,TANK_BODY_TIGER_Y*dm.density,TANK_GUN_TIGER_X*dm.density,TANK_GUN_TIGER_Y*dm.density,TANK_MAX_MOVE_DISTANCE_TIGER*dm.density,tigerType,dm);
		}
		return null;
	}
}
