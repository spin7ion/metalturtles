package org.spin7ion.MetalTurtles;

import org.spin7ion.MetalTurtles.physics.PhysicsRectange;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

public class House extends GameObject {

	public House(Bitmap graphics, float x, float y, float bodyCenterX,
			float bodyCenterY, DisplayMetrics metrics) {
		super(graphics, x, y, bodyCenterX, bodyCenterY, metrics);
		myPhysObject=new PhysicsRectange(x+bodyCenterX, y+bodyCenterY, bodyCenterX*2, bodyCenterY*2);
	}
}
