package org.spin7ion.MetalTurtles;

import org.spin7ion.MetalTurtles.controlElements.TankMoveElement;
import org.spin7ion.MetalTurtles.graphics.Camera;
import org.spin7ion.MetalTurtles.graphics.EffectHolder;
import org.spin7ion.MetalTurtles.physics.CoordsHistory;
import org.spin7ion.MetalTurtles.physics.PhysicsRectange;
import org.spin7ion.Utils.GeometryUtils;
import org.spin7ion.Utils.MathVector;
import org.spin7ion.Utils.Ray;
import org.spin7ion.Utils.Segment;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;

public class Tank extends GameObject {
	private GameObject gun;
	private TankMoveElement nextOrder;

	private boolean showOrder = false;
	private TankType tankType;

	private int hp;

	private CoordsHistory history;

	private MathVector nextPosition;
	private float nextAngle;

	private boolean needFire = false;
	private long canFireAt = 0;
	private EffectHolder effects=null;

	public static final int PhysicsSegmentFront = 2;
	public static final int PhysicsSegmentBack = 0;
	public static final int PhysicsSegmentLeft = 1;
	public static final int PhysicsSegmentRight = 3;

	public Tank(Bitmap bodyGraphics, Bitmap gunGraphics, Resources res,
			float x, float y, float bodyCenterX, float bodyCenterY,
			float gunCenterX, float gunCenterY, float maxDistance,
			TankType tankType, DisplayMetrics metrics) {
		super(bodyGraphics, x, y, bodyCenterX, bodyCenterY, metrics);

		myPhysObject = new PhysicsRectange(x, y,
				bodyGraphics.getScaledWidth(metrics),
				bodyGraphics.getScaledHeight(metrics));
		this.tankType = tankType;
		hp = tankType.getHp();
		gun = new GameObject(gunGraphics, x, y, gunCenterX, gunCenterY, metrics);
		nextOrder = new TankMoveElement(res, maxDistance, metrics, this);
		history = new CoordsHistory();
		nextPosition = getCenterCoords();
	}

	public Tank(Bitmap bodyGraphics, Bitmap gunGraphics, Resources res,
			float x, float y, float maxDistance, DisplayMetrics metrics) {
		super(bodyGraphics, x, y, metrics);
		gun = new GameObject(gunGraphics, x, y, metrics);
		nextOrder = new TankMoveElement(res, maxDistance, metrics, this);
		history = new CoordsHistory();
		nextPosition = getCenterCoords();
	}

	public void initOrder() {
		nextOrder.move(x + centerX - nextOrder.getCenterX(), y + centerY
				- nextOrder.getCenterY(), x, y);
		nextOrder.setAngle(getGunAngle());

	}

	public boolean isOrdershown() {
		return showOrder;
	}

	public void showOrder() {
		showOrder = true;
	}

	public void hideOrder() {
		showOrder = false;
	}

	public void toddleOrder() {
		showOrder = false;
	}

	public MathVector getCoords() {
		return new MathVector(getX(), getY());
	}

	public MathVector getCenterCoords() {
		return new MathVector(getX() + getCenterX(), getY() + getCenterY());
	}

	public MathVector getOrderCoords() {
		return nextOrder.getOrderCoords();
	}
	/**
	 * @return the nextOrder
	 */
	public TankMoveElement getNextOrder() {
		return nextOrder;
	}

	/**
	 * @param nextOrder
	 *            the nextOrder to set
	 */
	public void setNextOrder(TankMoveElement nextOrder) {
		this.nextOrder = nextOrder;
	}
	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		if (showOrder && hp > 0) {
			drawOrderLine(myCanvas, myCamera);
		}
		super.draw(myCanvas, myCamera, metrics);
		if (hp > 0) {
			gun.draw(myCanvas, myCamera, metrics);
		}
		//myPhysObject.draw(myCanvas, myCamera);
		//drawPhysEdges(myCanvas, myCamera);
	}

	public void drawOrderIfNeeden(Canvas myCanvas, Camera myCamera,
			DisplayMetrics metrics) {
		if (showOrder && hp > 0) {
			nextOrder.draw(myCanvas, myCamera, metrics);
		}
	}

	/*private void drawPhysCorners(Canvas myCanvas, Camera myCamera) {
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		MathVector[] a = ((PhysicsRectange) myPhysObject).getCorners();

		myCanvas.drawLine(myCamera.getDotScreenX(a[0].getX()),
				myCamera.getDotScreenY(a[0].getY()),
				myCamera.getDotScreenX(a[1].getX()),
				myCamera.getDotScreenY(a[1].getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[1].getX()),
				myCamera.getDotScreenY(a[1].getY()),
				myCamera.getDotScreenX(a[2].getX()),
				myCamera.getDotScreenY(a[2].getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[2].getX()),
				myCamera.getDotScreenY(a[2].getY()),
				myCamera.getDotScreenX(a[3].getX()),
				myCamera.getDotScreenY(a[3].getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[3].getX()),
				myCamera.getDotScreenY(a[3].getY()),
				myCamera.getDotScreenX(a[0].getX()),
				myCamera.getDotScreenY(a[0].getY()), mPaint);

	}

	private void drawPhysEdges(Canvas myCanvas, Camera myCamera) {
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		Segment[] a = ((PhysicsRectange) myPhysObject).getEdges();

		myCanvas.drawLine(myCamera.getDotScreenX(a[0].getX()),
				myCamera.getDotScreenY(a[0].getY()),
				myCamera.getDotScreenX(a[0].getEnd().getX()),
				myCamera.getDotScreenY(a[0].getEnd().getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[1].getX()),
				myCamera.getDotScreenY(a[1].getY()),
				myCamera.getDotScreenX(a[1].getEnd().getX()),
				myCamera.getDotScreenY(a[1].getEnd().getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[2].getX()),
				myCamera.getDotScreenY(a[2].getY()),
				myCamera.getDotScreenX(a[2].getEnd().getX()),
				myCamera.getDotScreenY(a[2].getEnd().getY()), mPaint);

		myCanvas.drawLine(myCamera.getDotScreenX(a[3].getX()),
				myCamera.getDotScreenY(a[3].getY()),
				myCamera.getDotScreenX(a[3].getEnd().getX()),
				myCamera.getDotScreenY(a[3].getEnd().getY()), mPaint);

	}*/

	private void drawOrderLine(Canvas canvas, Camera myCamera) {
		Paint mPaint = new Paint();
		mPaint.setColor(0xFFFF0000);
		canvas.drawLine(
				myCamera.getObjectCenterScreenX(this) * myCamera.getZoom(),
				myCamera.getObjectCenterScreenY(this) * myCamera.getZoom(),
				myCamera.getObjectCenterScreenX(this.getOrder())
						* myCamera.getZoom(),
				myCamera.getObjectCenterScreenY(this.getOrder())
						* myCamera.getZoom(), mPaint);
	}

	private void moveGun() {
		gun.move(this.getX() + this.centerX - gun.getCenterX(), this.getY()
				+ this.centerY - gun.getCenterY());
	}

	public void moveOrder(float x, float y) {
		nextOrder.move(x - nextOrder.getCenterX(), y - nextOrder.getCenterY(),
				getX(), getY());
	}

	public void moveGunOrder(float x, float y) {
		nextOrder.moveOrderGun(x, y);
	}

	public TankMoveElement getOrder() {
		return nextOrder;
	}

	public void move(float x, float y) {
		super.move(x, y);
		attachPhysObject();
		moveGun();
	}

	public void moveCenter(float x, float y) {
		super.moveCenter(x, y);
		attachPhysObject();
		moveGun();
	}

	public void moveCenter(MathVector newCoords) {
		super.moveCenter(newCoords);
		attachPhysObject();
		moveGun();
	}

	@Override
	public void setAngle(float d) {
		super.setAngle(d);
		attachPhysObject();
	}

	public void moveCenterAndRotate(MathVector newCoords, float d) {
		super.moveCenter(newCoords);
		super.setAngle(d);
		attachPhysObject();
		moveGun();
	}

	private void attachPhysObject() {
		myPhysObject.move(getX() + centerX, getY() + centerY);
		((PhysicsRectange) myPhysObject).setAngleDeg(this.getAngle());
	}

	public void rotateGun(float ang) {
		gun.rotate(ang);
	}

	public TankType getTankType() {
		return tankType;
	}

	public float getGunAngle() {
		return gun.getAngle();
	}

	public MathVector getGunDirection() {
		return new MathVector((float) Math.cos(GeometryUtils
				.degToRad(getGunAngle() - 90.f)),
				(float) Math.sin(GeometryUtils.degToRad(getGunAngle() - 90.f)));
	}

	public void setGunAngle(float a) {
		gun.setAngle(a);
	}

	public void addHistoryPoint() {
		history.addPoint(getCenterCoords(), getAngle(), getGunAngle(),
				System.currentTimeMillis());
	}

	public void moveToHistoryPoint(int id) {
		this.moveCenter(history.getHistoryPoint(id).getCoords());
	}

	public void calculateNextPosition() {
		// update needen body angle
		if (hp == 0) {
			return;
		}
		MathVector currentCoords = getCenterCoords();

		// move if turned
		float angleDiff = GeometryUtils.normalizeAngle(getOrder()
				.getBodyAngle() - getAngle());
		if ((angleDiff > -10 && angleDiff < 10)
				|| (angleDiff < 190 && angleDiff > 170)) {

			if (currentCoords.sub(getOrderCoords()).lenght() < this
					.getTankType().getMoveSpeed()) {
				nextPosition = getOrderCoords();
			} else {
				nextPosition = getOrderCoords().sub(currentCoords)
						.norm(getTankType().getMoveSpeed()).add(currentCoords);
			}
		}
		// turn tank
		float sub = getOrder().getBodyAngle() - getAngle();
		sub = GeometryUtils.normalizeAngle(sub);
		if (Math.abs(sub) > 120) {
			sub = -180 + sub;
		}
		sub = GeometryUtils.normalizeAngle(sub);

		if (Math.abs(sub) < getTankType().getBodyTurnSpeed()) {
			nextAngle = getAngle() + sub;
		} else {
			nextAngle = getAngle() + Math.signum(sub)
					* getTankType().getBodyTurnSpeed();
		}
	}

	public PhysicsRectange getNextPhysicsRectange() {
		PhysicsRectange ret = (PhysicsRectange) this.getPhysicsObject().clone();
		ret.setAngleDeg(nextAngle);
		ret.move(nextPosition.getX(), nextPosition.getY());
		return ret;
	}

	public void setNextPosition(MathVector nextPosition) {
		this.nextPosition = nextPosition;
	}

	public void setNextAngle(float a) {
		this.nextAngle = a;
	}

	public void turnGun() {
		float sub = getOrder().getGunAngle() - getGunAngle();

		sub = GeometryUtils.normalizeAngle(sub);
		if (Math.abs(sub) > 180) {
			sub += Math.signum(sub) * 180;
		}

		if (Math.abs(sub) < getTankType().getGunTurnSpeed()) {
			setGunAngle(getGunAngle() + sub);
			getOrder().setGunTurnMade(true);
		} else {
			setGunAngle(getGunAngle() + Math.signum(sub)
					* getTankType().getGunTurnSpeed());
		}
	}

	public void applyNextPosition() {
		moveCenterAndRotate(nextPosition, nextAngle);
	}

	public boolean orderFire() {
		if (canFireAt < System.currentTimeMillis()) {
			needFire = true;
		}
		return needFire;
	}

	public void untriggerFire() {
		canFireAt = System.currentTimeMillis() + tankType.getReloadTime();
		needFire = false;
	}

	public void cancelFire() {
		needFire = false;
	}

	public boolean needFire() {
		return needFire;
	}

	public long getReloadTime() {
		return tankType.getReloadTime();
	}

	public long getReloadedAlreadyTime() {
		long elapsed = canFireAt - System.currentTimeMillis();
		if (elapsed < 0)
			return getReloadTime();
		return getReloadTime() - elapsed;
	}

	public Ray getFireRay() {
		return new Ray(getCenterCoords(), getGunDirection());
	}

	public int hit(int baseDamage, int hitSegment, float hitAngle) {
		if (hitSegment == PhysicsSegmentFront) {
			hp -= baseDamage;
		} else if (hitSegment == PhysicsSegmentBack) {
			hp -= baseDamage * 2;
		} else {
			hp -= baseDamage * 1.5;
		}
		if (hp <= 0) {

			int width, height;
			height = getGraph().getHeight();
			width = getGraph().getWidth();

			Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
					Bitmap.Config.RGB_565);
			Canvas c = new Canvas(bmpGrayscale);
			Paint paint = new Paint();
			ColorMatrix cm = new ColorMatrix();
			cm.setSaturation(0);
			
			ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
			paint.setColorFilter(f);
			c.drawBitmap(getGraph(), 0, 0, paint);
			setGraph(bmpGrayscale);
			if(effects!=null){
				effects.addSmoke(getX(),getY(),true);
			}
			hp = 0;
		}

		return hp;
	}

	public int getHp() {
		return hp;
	}

	public void addEffectHolder(EffectHolder effects) {
		this.effects=effects;
	}
}
