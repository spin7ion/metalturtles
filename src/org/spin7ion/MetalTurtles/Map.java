package org.spin7ion.MetalTurtles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.spin7ion.MetalTurtles.graphics.Camera;
import org.spin7ion.MetalTurtles.physics.Clip;
import org.spin7ion.MetalTurtles.physics.PhysicsObject;
import org.spin7ion.MetalTurtles.physics.PhysicsRectange;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;

public class Map {

	public static final int PRIORITY_GRASS = 0;
	public static final int PRIORITY_HOUSES = 1;
	public static final int PRIORITY_TREES = 2;
	public static final int PRIORITY_DEBUG_CLIPS = 3;

	Bitmap ground;

	private List<House> houses = new ArrayList<House>();
	private List<Clip> clips = new ArrayList<Clip>();

	private DisplayMetrics mMetrics;
	private Camera mCamera;

	public Map(Camera mCamera, Resources res, DisplayMetrics dm)
			throws XmlPullParserException, IOException {
		this.mMetrics = dm;
		this.mCamera = mCamera;
		XmlResourceParser xpp = res.getXml(R.xml.map_desert);
		xpp.next();
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			
			eventType = xpp.next();
		}
		ground=BitmapFactory.decodeResource(res, R.drawable.map_desert);
		// clips.add(new Clip(new PhysicsRectange(0, 0, 10, 10), true));
		// houses.add(HouseFactory.createHouse(HouseFactory.HOUSE_PENTHOUSE,
		// res,mMetrics));
	}

	public List<House> getHouses() {
		return houses;
	}

	public List<Clip> getClips() {
		return clips;
	}

	public void update() {

	}

	public void draw(int priority, Canvas canvas) {
		switch (priority) {
		case PRIORITY_GRASS:
			// draw Bitmap
			
			//Rect srcRect=new Rect((int)(mCamera.getCameraX()-mMetrics.widthPixels/2),(int)(mCamera.getCameraY()-mMetrics.heightPixels/2),(int)(mCamera.getCameraX()+mMetrics.widthPixels/2),(int)(mCamera.getCameraY()+mMetrics.heightPixels/2));
			//canvas.drawBitmap(ground, srcRect, new Rect( 0,0,mMetrics.widthPixels,mMetrics.heightPixels), null);
			break;
		case PRIORITY_HOUSES:
			synchronized (houses) {
				Iterator<House> iterator = houses.iterator();
				while (iterator.hasNext()) {
					House house = iterator.next();
					house.draw(canvas, mCamera, mMetrics);
				}
			}
			break;
		case PRIORITY_TREES:

			break;
		case PRIORITY_DEBUG_CLIPS:
			synchronized (clips) {
				Iterator<Clip> iterator = clips.iterator();
				while (iterator.hasNext()) {
					iterator.next().getPhysicsObject().draw(canvas, mCamera);
				}
			}
			break;
		}
	}
}
