package org.spin7ion.MetalTurtles;

import org.spin7ion.MetalTurtles.R;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

public class HouseFactory {
	
	public static int HOUSE_PENTHOUSE=0;
	
	public static House createHouse(int houseType,Resources res,DisplayMetrics metrics){
		if(houseType==HOUSE_PENTHOUSE){
			return new House(BitmapFactory.decodeResource(res,
					R.drawable.penthouse), 0, 0, 114*metrics.density, 47.5f*metrics.density, metrics);
		}
		return null;
	}
}
