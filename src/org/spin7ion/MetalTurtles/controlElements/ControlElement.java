package org.spin7ion.MetalTurtles.controlElements;

import org.spin7ion.MetalTurtles.graphics.BitmapObject;

import android.graphics.Bitmap;
import android.util.DisplayMetrics;

public class ControlElement extends BitmapObject {
	private ControlElementOnClickListener myOnclickListener;
	
	public ControlElement(Bitmap graphics, float x, float y, DisplayMetrics dm) {
		super(graphics, x, y,dm);
		this.setAbsolute();
	
	}
	
	public boolean checkClick(int x,int y){
		if(isPointCollide(x,y,metrics)){
			myOnclickListener.onControlElementClick(new ControlElementEvent(this,x,y));
			return true;
		}
		return false;
	}
	
	public void setControlElementOnClickListener(ControlElementOnClickListener listener){
		myOnclickListener=listener;
	}
	public ControlElementOnClickListener getControlElementOnClickListener(){
		return myOnclickListener;
	}

}
