package org.spin7ion.MetalTurtles.controlElements;

import org.spin7ion.MetalTurtles.graphics.Camera;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.DisplayMetrics;

public class HpGauge extends Gauge {

	float width;
	float height;
	
	private static float radius=10;
	
	Paint paint = new Paint();
	
	public HpGauge(float currentValue, float maxValue, float x2, float y2,float width,float height,
			DisplayMetrics metrics) {
		super(currentValue, maxValue, x2, y2, metrics);
		this.width=width;
		this.height=height;
		radius=radius*metrics.density;
	}

	@Override
	public void draw(Canvas canvas, Camera myCamera, DisplayMetrics metrics) {
		//Shader gradient = new RadialGradient(myCamera.getObjectScreenX(this), myCamera.getObjectScreenY(this), radius*metrics.density+width, Color.CYAN, Color.RED, Shader.TileMode.MIRROR);
		Shader gradient = new LinearGradient(myCamera.getObjectScreenX(this)*myCamera.getZoom(), myCamera.getObjectScreenY(this)*myCamera.getZoom(), myCamera.getObjectScreenX(this)*myCamera.getZoom(), myCamera.getObjectScreenY(this)*myCamera.getZoom()+radius*metrics.density*myCamera.getZoom()+width*myCamera.getZoom(), Color.GREEN, Color.RED, Shader.TileMode.MIRROR);
		paint.setShader(gradient);
		paint.setStrokeWidth(5);
	    paint.setAntiAlias(false);
	    paint.setStrokeCap(Paint.Cap.ROUND);
	    paint.setStyle(Paint.Style.STROKE);
		canvas.drawArc(new RectF(myCamera.getObjectScreenX(this)*myCamera.getZoom()-radius*myCamera.getZoom()*metrics.density, myCamera.getObjectScreenY(this)*myCamera.getZoom()-radius*myCamera.getZoom()*metrics.density, myCamera.getObjectScreenX(this)*myCamera.getZoom()+width*myCamera.getZoom()+radius*myCamera.getZoom()*metrics.density, myCamera.getObjectScreenY(this)*myCamera.getZoom()+height*myCamera.getZoom()+radius*myCamera.getZoom()*metrics.density), 90, -180*this.getFillFactor(), false, paint);
	}

}
