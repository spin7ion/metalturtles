package org.spin7ion.MetalTurtles.controlElements;

public class ControlElementEvent {
	
	private ControlElement cElement;
	private int x,y;
	
	public ControlElementEvent(ControlElement ce,int x,int y){
		cElement=ce;
		this.x=x;
		this.y=y;
	}
	public ControlElement getControlElement(){
		return cElement;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
}
