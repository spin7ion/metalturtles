package org.spin7ion.MetalTurtles.controlElements;

import org.spin7ion.MetalTurtles.graphics.Camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;

public class ReloadGauge extends Gauge {
	
	private Bitmap bitmap;
	private Rect srcAllRect = null;
	
	public ReloadGauge(float currentValue, float maxValue, float x2, float y2, DisplayMetrics metrics,Bitmap bitmap) {
		super(currentValue, maxValue, x2*metrics.density, y2*metrics.density, metrics);
		
		this.setBitmap(bitmap);
		
		
		srcAllRect=new Rect(0,0,(int)(46*metrics.density),(int)(56*metrics.density));		
	}


	public void draw(Canvas myCanvas, Camera myCamera, DisplayMetrics metrics) {
		
		
		Rect dstRect=new Rect((int)(myCamera.getObjectScreenX(this)*myCamera.getZoom()),
				(int)(myCamera.getObjectScreenY(this)*myCamera.getZoom()),
				(int)((myCamera.getObjectScreenX(this)+getWidth())*myCamera.getZoom()),
				(int)((myCamera.getObjectScreenY(this)+getHeight())*myCamera.getZoom()));
		
		myCanvas.drawBitmap(getBitmap(), srcAllRect, dstRect, null);
		
		Rect fillRect= new Rect((int)(49*metrics.density),(int) ((56*getNotFillFactor())*metrics.density),(int)(95*metrics.density),(int)(55*metrics.density));
		Rect dstFillRect= new Rect(dstRect.left,
				(int) (dstRect.top+getWidth()*getNotFillFactor()),
				dstRect.right,dstRect.bottom);
		myCanvas.drawBitmap(getBitmap(),fillRect, dstFillRect, null);
	}
	public float getWidth(){
		return 23*metrics.density;
	}
	public float getHeight(){
		return 28*metrics.density;
	}
	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

}
