package org.spin7ion.MetalTurtles.controlElements;

public interface ControlElementOnClickListener{
	public void onControlElementClick(ControlElementEvent event);
}
