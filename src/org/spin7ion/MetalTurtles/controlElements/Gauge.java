package org.spin7ion.MetalTurtles.controlElements;

import org.spin7ion.MetalTurtles.graphics.GraphicObject;

import android.util.DisplayMetrics;

public abstract class Gauge extends GraphicObject {

	float currentValue;
	float maxValue;

	
	
	public Gauge(float currentValue,float maxValue,float x2, float y2, DisplayMetrics metrics) {
		super(x2, y2, metrics);
		this.maxValue=maxValue;
		this.currentValue=currentValue;
	}
	public float getFillFactor(){
		return currentValue/maxValue;
	}
	public float getNotFillFactor(){
		return 1.f-currentValue/maxValue;
	}
	public float getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(long currentValue) {
		this.currentValue = currentValue;
	}
	public float getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(long maxValue) {
		this.maxValue = maxValue;
	}
}
