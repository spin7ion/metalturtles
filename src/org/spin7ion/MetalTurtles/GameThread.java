package org.spin7ion.MetalTurtles;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameThread extends Thread {
    private GameContainer mGameContainer;
    private boolean mRun;
 
    public GameThread(GameContainer gameContainer) {
    	mGameContainer = gameContainer;        
    }
 
    public void setRunning(boolean run) {
        mRun = run;
    }
 
    @Override
    public void run() {        
        while (mRun) {  
        	
        	try {
				sleep(mGameContainer.update());
			} catch (InterruptedException e) { 
				e.printStackTrace();
			}
        }
    }
}
