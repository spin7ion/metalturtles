package org.spin7ion.Utils;

public class Line extends MathVector {

	MathVector dir;
	//r=dot+dir*t
	public Line(MathVector dot, MathVector dir) {
		super(dot.getX(), dot.getY());
		this.dir = dir;
		// a=(dir.getY()*dot.getX()-dir.getX()*dot.getY())/dir.getY();
		// b=(dir.getX()*dot.getY()-dir.getY()*dot.getX())/dir.getX();
	}
	public MathVector getDirection(){
		return dir;
	}
	public boolean isCollides(Line test) {
		return ((test.getX()-getX())/(dir.getX()-test.getDirection().getX()))==((test.getY()-getY())/(dir.getY()-test.getDirection().getY()));
	}

	public MathVector getCollision(Line test) {
		
		float   x1 = this.getX(), y1 = this.getY(),
	            x2 = this.add(getDirection()).getX(), y2 = this.add(getDirection()).getY(),
	            x3 = test.getX(), y3 = test.getY(),
	            x4 = test.add(test.getDirection()).getX(), y4 = test.add(test.getDirection()).getY();
		MathVector intersection=new MathVector(); 
	    intersection.setX(GeometryUtils.det(GeometryUtils.det(x1, y1, x2, y2), x1 - x2,
	    		GeometryUtils.det(x3, y3, x4, y4), x3 - x4)/
	                         GeometryUtils.det(x1 - x2, y1 - y2, x3 - x4, y3 - y4));
	    intersection.setY(GeometryUtils.det(GeometryUtils.det(x1, y1, x2, y2), y1 - y2,
	    		GeometryUtils.det(x3, y3, x4, y4), y3 - y4)/
	                         GeometryUtils.det(x1 - x2, y1 - y2, x3 - x4, y3 - y4));
	    
	    return intersection;
	}
}
