package org.spin7ion.Utils;

import android.util.Log;

public class GeometryUtils {
	public static float getAngleRad(float ax,float ay,float bx,float by){
		return (float) (Math.atan2(by-ay,bx-ax) - Math.atan2(1,0));
	}
	public static float getAngleDeg(float ax,float ay,float bx,float by){
		return radToDeg( getAngleRad(ax,ay,bx,by));
	}
	public static float degToRad(float a){
		return (float)(Math.PI*a/180.f);
	}
	public static float radToDeg(float a){
		return (float)(180.f*a/Math.PI);
	}
	public static double radToDeg(double a) {
		return (180.f*a/Math.PI);
	}
	public static float normalizeAngle(float ang){
		while(Math.abs(ang)>360){
			ang-=Math.signum(ang)*360.f;}
		return ang;
	}
	public static float minAbs(float a,float b){
		if(Math.abs(a)>Math.abs(b)){
			return b;
		}
		return a;
	}
	public static float maxAbs(float a,float b){
		if(Math.abs(a)>Math.abs(b)){
			return a;
		}
		return b;
	}
	public static MathVector max(MathVector a,MathVector b){
		if(a.lenght()>b.lenght()){
			return a;
		}
		return b;
	}
	public static MathVector min(MathVector a,MathVector b){
		if(a.lenght()>b.lenght()){
			return b;
		}
		return a;
	}
	static float det(float a, float b, float c, float d)
	{
	    return a * d - b * c;
	}
	

}
