package org.spin7ion.Utils;

public class Segment extends Line{
	MathVector end;
	public Segment(MathVector a, MathVector b) {
		super(a, a.sub(b));
		end=b;
	}
	
	public MathVector getCollision(Line test) {		
		MathVector collisionDot=super.getCollision(test);
		
		if(collisionDot!=null && collisionDot.sub(end).isLessThan(this) && collisionDot.sub(end).isLessThan(end)){
			return collisionDot;
		}
		return null;
	}
	public MathVector getCollision(Ray test) {
		MathVector collisionDot=test.getCollision(this);
		
		if(collisionDot!=null && this.sub(collisionDot).lenght()+end.sub(collisionDot).lenght()<=this.sub(end).lenght()){
			return collisionDot;
		}
		return null;
	}
	public MathVector getEnd(){
		return end;
	}
}
