package org.spin7ion.Utils;

public class MathVector {
	private float x,y;

	public MathVector(){
		this.x=0;
		this.y=0;
	}
	
	public MathVector(float x, float y){
		this.x=x;
		this.y=y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public boolean isMoreThan(MathVector test){
		return x*x+y*y>test.getX()*test.getX()+test.getY()*test.getY();
	}
	
	public boolean isLessThan(MathVector test){
		return x*x+y*y<test.getX()*test.getX()+test.getY()*test.getY();
	}
	
	public boolean isMoreEqThan(MathVector test){
		return x*x+y*y>=test.getX()*test.getX()+test.getY()*test.getY();
	}
	
	public boolean isLessEqThan(MathVector test){
		return x*x+y*y<=test.getX()*test.getX()+test.getY()*test.getY();
	}
	
	public MathVector add(MathVector a){
		return new MathVector(x+a.getX(), y+a.getY());
	}
	
	public MathVector sub(MathVector a){
		return new MathVector(x-a.getX(), y-a.getY());
	}
	
	public MathVector mult(float a){
		return new MathVector(x*a, y*a);
	}
	
	public float dot(MathVector b){
		return this.getX()*b.getX()+this.getY()*b.getY();
	}
	
	public MathVector neg(){
		return new MathVector(-x, -y);
	}
	
	public MathVector rotate(float angle){
		float cos=(float) Math.cos(angle);
		float sin=(float) Math.sin(angle);
		return new MathVector(x*cos+y*sin, x*sin-y*cos);
	}
	
	public float lenght(){
		return (float) Math.sqrt(x*x+y*y);
	}

	@Override
	public String toString() {
		return "MathVector [x=" + x + ", y=" + y + "]";
	}
	/**
	 * 
	 * @return 
	 */
	public MathVector ort() {
		return new MathVector(-y, x);
	}
	
	public MathVector norm() {
		return new MathVector(x/this.lenght(), y/this.lenght());
	}
	
	//norm on length
	public MathVector norm(float length) {
		return norm().mult(length);
	}

	public MathVector setLenght(float a) {
		return new MathVector(x*a/this.lenght(), y*a/this.lenght());
	}

	public boolean isEq(MathVector norm) {
		return Math.abs(x-norm.x)<0.001 && Math.abs(y-norm.y)<0.001;
	}
	public double getAngleBetween(MathVector b){
		 return Math.atan2(getX()*b.getY() - b.getX()*getY(), dot(b));
	}
	public double getCosBetween(MathVector b){
		 return dot(b);
	}
}
