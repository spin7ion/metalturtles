package org.spin7ion.Utils;

public class Ray extends Line {

	public Ray(MathVector dot, MathVector dir) {
		super(dot, dir);
	}
	public MathVector getCollision(Line test) {
		MathVector collisionDot=super.getCollision(test);		
		
		if(collisionDot!=null && collisionDot.sub(this).norm().isEq(dir.norm())){
			return collisionDot;
		}
		return null;
	}

}
